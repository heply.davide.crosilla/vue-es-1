module.exports = {
  content: ['./src/**/*.vue'],
  theme: {
    extend: {
      spacing: {
        '116': '29rem'
      }
    },
  },
  plugins: [],
}
