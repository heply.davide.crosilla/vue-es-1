# vue-es-1

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
npx tailwindcss -i ./src/main.css -o ./public/output.css --watch
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
